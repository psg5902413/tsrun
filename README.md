# tsrun

tsrun is an enhanced CLI for Test Studio's ArtOfTest.Runner, which is the Windows command-line tool for launching and running tests and test lists created with Telerik Test Studio.

## Description

tsrun is quite simply a front-end for (or a wrapper around) the ArtOfTest.Runner tool. It does not replace ArtOfTest.Runner and depends on it also being installed on the machine running the tests.

tsrun aims to provide:

1. a modern and consistent CLI
2. the ability to set and override individual test and test list settings on a per-run basis
3. relaxed requirements for entering test, list, and settings file names/paths
4. the ability to auto-load a settings file
5. the ability to specify a custom chrome profile for tests running in Chrome using the browser extension

## Installation

To install tsrun, simply copy the executable file to any directory on the machine, preferably one that is already in the machine's PATH variable if you want to be able to run it conveniently regardless of the current working directory.

You can also copy it to the same directory where ArtOfTest.Runner.exe is located, though this is not necessary. When running tsrun, it will attempt to automatically locate ArtOfTest.Runner.exe using the following criteria:

1. Use the full path specified by the --runner-path option, if specified
2. Check the path specified in the AOTR_PATH environment variable, if defined
3. Attempt to launch ArtOfTest.Runner.exe relying on an OS search (current dir. then PATH var.)
4. Lookup and use the value of the Test Studio BinPath entry in the Windows registry

## Usage

![tsrun_usage](tsrun_usage.png)

All of the options supported by ArtOfTest.Runner are provided along with both short and long names for each of these options (hence the modern and consistent CLI à la the [clap](https://crates.io/crates/clap) crate).

Additionally, tsrun provides the `set` subcommand for setting or overriding test and test list settings on a per-run basis. Typing `tsrun help set` will display a list of all of settings along with short descriptions and possible values for each. Essentially, all of the settings that can be specified for a test list in the Test Studio GUI are accounted for here.

### Using a settings file

Typically, a test settings file (.json) can be specified with the `--settings` option. tsrun provides a shortcut if you always use the same settings file for a specific test or test list: just give the settings file the same base name as the test or test list and you can omit specifying it explicitly on the command line.

For example, if you have a settings file named MySettings.json that you always use with MyTest.tstest, simply rename MySettings.json to MyTest.json and tsrun will automatically use the settings in this file. Any further settings specified with the `set` command will be combined with (or will override) the settings in this file for the current run only.

To suppress the automatic loading of a settings file for a run, specify the `--disable-auto-settings-load` option.

### Specifying a custom Chrome profile

One setting that is available with the `set` command that is not a regular Test Studio setting is the `--chrome-profile-name` setting. Prior to having this setting available with tsrun, the only way to use a custom Chrome profile when running a test with Test Studio would be to manually make a change in the Windows registry prior to running the test.

To specify a custom Chrome profile with tsrun, the profile must first have been created per the instructions in [this knowledge base article](https://docs.telerik.com/teststudio/knowledge-base/browsers-kb/custom-chrome-profile). However, it is not necessary to keep the ChromeProfileName setting defined in the registry for running tests with tsrun; tsrun will temporarily set this registry value to the value specified for `--chrome-profile-name` at the start of a run, and then change it back to its previous value (if any) once the run completes.

```
tsrun -t MyTest set --browser=chrome --chrome-profile-name=TestProfile1
```

__Note:__ It is currently only possible to use a custom Chrome profile when using the Test Studio browser extension, but it is not necessary to explicitly specify the `--use-browser-extension` setting when specifying `--chrome-profile-name`, it is automatically implied. Hopefully in the future, custom Chrome profiles will be supported also for extension-less test execution (extension-less recording/execution became the default mode with the release of [Test Studio R2 2023](https://www.telerik.com/support/whats-new/teststudio/release-history/test-studio-r2-2023)).


## Examples

### Run a test (.tstest)

```
C:\MyTests\tsrun -t MyWebTest
```

Note that as long as the .tstest file exists in the current directory, it is not necessary to specify a path to the file, nor is it necessary to specify the file extension (unless it differs from .tstest).

### Run a test using a specific browser

```
C:\MyTests\tsrun -t MyWebTest set --browser chrome
```

Note that an equals sign between the option/setting and its value is optional for both short and long names. The following is also acceptable:

```
C:\MyTests\tsrun -t=MyWebTest set --browser=chrome
```

### Run a test list (.aiilist)

```
C:\MyTests\tsrun --list MyTestList
```

The rules for specifying a path and file extension are the same as when specifying a test file with one added convenience: if the test list file is not found in the current directory, tsrun will check to see if it exists in a subdirectory named `TestLists`.

### Specify a settings file (.json)

Let's assume you have the following settings file:

__MySettings.json__
```
{
  "Settings": {
    "__type": "ArtOfTest.WebAii.Core.Settings",
    "__value": {
      "Web": {
        "__type": "ArtOfTest.WebAii.Core.Settings+WebSettings",
        "__value": {
          "DefaultBrowser": 7
        }
      },
	  "AnnotateExecution": true,
      "CreateLogFile": false
    }
  }
}
```

This file sets the browser to Chrome, enables runtime annotations, and disables logging.

_Side note: How do we know that `7` is the id for Chrome? Don't ask! With tsrun, we don't need to know about implementation details like browser ids, we can simply specify `chrome` on the command line when setting a browser._

To use this file when running a test or test list:

```
tsrun -t MyTest --settings MySettings
```

The rules for specifying a settings file path and extension are the same as for specifying a test file.

### Override settings defined in a settings file

Assuming the MySettings.json file listed above, disable annotations for the current run and run the test in Edge:

```
tsrun --test MyWebTest --settings MySettings set --annotate-execution=false --browser=edge
```

### Auto-load settings from a settings file

Let's say that you always want to use the settings defined in MySettings.json when running MyWebTest.tstest. Instead of specifying this with the `--settings` option each time, simply rename MySettings.json to MyWebTest.json and run the test:

```
tsrun --test MyWebTest
```

The settings in MyWebTest.json are automatically loaded and applied. Any further settings specified on the command line using the `set` command will be applied as usual:

```
tsrun --test MyWebTest set --annotate-execution=false --browser=edge
```
