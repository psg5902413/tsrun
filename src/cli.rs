use clap::{Args, Parser, Subcommand, ValueEnum};
use std::path::PathBuf;

//======================================================================
// Main CLI definition
//======================================================================
/// Enhanced CLI for Test Studio's ArtOfTest.Runner
#[derive(Parser, Debug)]
#[command(author, version, verbatim_doc_comment)]
#[command(styles = get_styles())]
#[command(help_template = "\
{before-help}{name} {version}
{about-with-newline}
{usage-heading} {usage}

{all-args}{after-help}
")]
pub struct Cli {
    //--------------------------------------
    // Standard ArtOfTest.Runner parameters
    //--------------------------------------
    #[command(flatten)]
    pub test_type: TestTypes,

    /// Adds an xml result file (only valid with list option)
    #[arg(long, short, conflicts_with = "test")]
    pub xml: bool,

    /// Adds an html result file (only valid with list option)
    #[arg(long, short = 'H', conflicts_with = "test")]
    pub html: bool,

    /// Adds a JUnit XML result file
    #[arg(long, short)]
    pub junit: bool,

    /// Adds a JUnit XML result file, where each test step is converted to a JUnit test
    #[arg(long, short = 'J')]
    pub junitstep: bool,

    /// The alternative output run result path
    #[arg(long, short, value_name = "PATH")]
    pub out: Option<String>,

    /// The alternative result file name
    #[arg(long, short, value_name = "FILE")]
    pub result: Option<String>,

    /// The path to the project root folder
    #[arg(long, short = 'R', value_name = "PATH")]
    pub root: Option<PathBuf>,

    /// Save the result after each step of the test
    #[arg(long, short)]
    pub persist_on_each_step: bool,

    /// The name or path to a .json file containing custom parameters/settings for the run
    #[arg(long, short, value_name = "FILE")]
    pub settings: Option<PathBuf>,

    //--------------------------------------
    // tsrun-specific options
    //--------------------------------------
    /// The full path to ArtOfTest.Runner.exe
    #[arg(long, short = 'P', value_name = "PATH", env = "AOTR_PATH")]
    pub runner_path: Option<PathBuf>,

    /// Do not automatically load settings file
    #[arg(short, long)]
    pub disable_settings_auto_load: bool,

    /// Print debugging information to stderr
    #[arg(long, hide = true)]
    pub debug: bool,

    // Commands
    #[command(subcommand)]
    pub command: Option<Commands>,
}

//======================================================================
// Commands
//======================================================================
#[derive(Subcommand, Debug)]
pub enum Commands {
    /// Set or override test settings
    Set(SetArgs),
}

//======================================================================
// ArgGroups
//======================================================================
#[derive(Args, Debug)]
#[group(required = true, multiple = false)]
pub struct TestTypes {
    /// The name or path to the test (.tstest) to run
    #[arg(long, short, value_name = "FILE", visible_alias = "aii")]
    pub test: Option<PathBuf>,

    /// The name or path to the test list (.aiilist) to run
    #[arg(long, short, value_name = "FILE")]
    pub list: Option<PathBuf>,
}

#[derive(Args, Debug)]
pub struct SetArgs {
    //----------------------------
    // General.Annotation
    //----------------------------
    /// Highlights and annotates target elements
    #[arg(long, value_name = "BOOL", display_order = 0)]
    pub annotate_execution: Option<bool>,

    /// The annotation mode to use when annotation is enabled
    #[arg(long, value_enum, display_order = 0)]
    pub annotation_mode: Option<AnnotationMode>,

    //----------------------------
    // General.Desktop
    //----------------------------
    /// Execute tests marked as In Development
    #[arg(long, value_name = "BOOL", display_order = 0)]
    pub execute_in_development_tests: Option<bool>,

    /// The simulated mouse movement speed in pixels/msec (typically between 0.1 - 0.5).
    #[arg(long, value_name = "MSEC", display_order = 0)]
    pub simulated_mouse_move_speed: Option<f32>,

    //----------------------------
    // General.Execution
    //----------------------------
    /// The timeout to wait for a client to be ready after initial launch and after
    /// executing a command
    #[arg(long, value_name = "MSEC", display_order = 0)]
    pub client_ready_timeout: Option<u32>,

    /// Disable handling of dialog windows
    #[arg(long, value_name = "BOOL", display_order = 0)]
    pub disable_dialog_monitoring: Option<bool>,

    /// The timeout to wait before starting the backup image search
    #[arg(long, value_name = "MSEC", display_order = 0)]
    pub element_image_search_delay: Option<u32>,

    /// The timeout for searching the image on the page
    #[arg(long, value_name = "MSEC", display_order = 0)]
    pub element_image_search_timeout: Option<u32>,

    /// The maximum time to wait for an element to load on the page
    #[arg(long, value_name = "MSEC", display_order = 0)]
    pub element_wait_timeout: Option<u32>,

    /// Use recorded element images as backup search method
    #[arg(long, value_name = "BOOL", display_order = 0)]
    pub enable_image_search: Option<bool>,

    /// The maximum time to wait for a command request to execute
    #[arg(long, value_name = "MSEC", display_order = 0)]
    pub execute_command_timeout: Option<u32>,

    /// The time to wait between commands
    ///
    /// Note: some steps contain multiple commands.
    #[arg(long, value_name = "MSEC", display_order = 0)]
    pub execution_delay: Option<u32>,

    /// Automatically re-run failed tests
    #[arg(long, value_name = "BOOL", display_order = 0)]
    pub rerun_failed_tests: Option<bool>,

    /// The time to wait for a response from ArtOfTest.Runner
    ///
    /// The purpose of this timeout is to terminate the runner if it hangs for some reason.
    /// A value of 0 disables the timeout. Note: the timer is reset for each test in a test list.
    #[arg(long, value_name = "MSEC", display_order = 0)]
    pub runner_response_timeout: Option<u32>,

    /// Scroll the page when searching an element by image
    #[arg(long, value_name = "BOOL", display_order = 0)]
    pub scroll_on_image_search: Option<bool>,

    /// Search by image before searching by element's find expression
    #[arg(long, value_name = "BOOL", display_order = 0)]
    pub search_by_image_first: Option<bool>,

    /// The action the DialogMonitor should take when it encounters unexpected dialogs
    #[arg(long, value_enum, display_order = 0)]
    pub unexpected_dialog_action: Option<UnexpectedDailogAction>,

    /// The time to wait between checks for all the Wait.For methods
    #[arg(long, value_name = "MSEC", display_order = 0)]
    pub wait_check_interval: Option<u32>,

    /// Use WebAii 1.1 style connections
    ///
    /// When set, multiple Manager objects can co-exist on the machine.
    #[arg(long, value_name = "BOOL", display_order = 0)]
    pub x_multi_mgr: Option<bool>,

    //----------------------------
    // General.Logging
    //----------------------------
    /// Enable general logging
    #[arg(long, value_name = "BOOL", display_order = 0)]
    pub create_log_file: Option<bool>,

    /// Log annotations to the log file
    #[arg(long, value_name = "BOOL", display_order = 0)]
    pub log_annotations: Option<bool>,

    /// The path to the log folder
    #[arg(long, value_name = "PATH", display_order = 0)]
    pub log_location: Option<String>,

    //----------------------------
    // General.Screen recording
    //----------------------------
    /// Recording Codec
    #[arg(long, value_enum, display_order = 0)]
    pub recording_codec: Option<RecordingCodec>,

    /// The number of captured video frames per second (lower = smaller files)
    #[arg(long, value_name = "FPS", display_order = 0)]
    pub recording_fps: Option<u16>,

    /// The screen recording mode
    #[arg(long, value_enum, display_order = 0)]
    pub recording_mode: Option<RecordingMode>,

    /// The path to save recorded video files
    #[arg(long, value_name = "PATH", display_order = 0)]
    pub recording_output_location: Option<String>,

    /// Set the downscaling of the recorded video from 10 to 100 percent
    #[arg(long, value_name = "PERCENT", value_parser = clap::value_parser!(u8).range(10..=100), display_order = 0)]
    pub recording_scale: Option<u8>,

    /// The maximum size in megabytes of the recorded video file
    ///
    /// Set to 0 for unlimited size. If the limit is reached, the video recording will be stopped
    /// before the test execution ends.
    #[arg(long, value_name = "MB", display_order = 0)]
    pub recording_size_limit: Option<u64>,

    //----------------------------
    // General.Translators
    //----------------------------
    /// The version of the Telerik Components being tested
    #[arg(long, value_enum, display_order = 0)]
    pub telerik_components_version: Option<TelerikComponentsVersion>,

    //----------------------------
    // Web.ASP.NET
    //----------------------------
    /// The development server port to use
    #[arg(long, value_name = "PORT", allow_negative_numbers = true, value_parser = clap::value_parser!(i32).range(-1..=65535), display_order = 0)]
    pub asp_net_dev_server_port: Option<i32>,

    /// The local web server to use
    ///
    /// When set to AspNetDevelopmentServer, the ASP.NET development server is started.
    #[arg(long, value_enum, display_order = 0)]
    pub local_web_server: Option<LocalWebServer>,

    /// The path to the application to test when running under the ASP.NET development server
    #[arg(long, value_name = "PATH", display_order = 0)]
    pub web_app_physical_path: Option<String>,

    //----------------------------
    // Web.Browser
    //----------------------------
    /// Automatically calibrate the browser(s) prior to test execution
    #[arg(long, value_name = "BOOL", display_order = 0)]
    pub auto_calibrate_browsers: Option<bool>,

    /// The default browser to launch
    #[arg(long, value_enum, display_order = 0)]
    pub browser: Option<Browser>,

    /// Debug non-UI page requests using a UI browser, like IE
    #[arg(long, value_name = "BOOL", display_order = 0)]
    pub enable_ui_less_request_viewing: Option<bool>,

    /// Force kill the browser process when closing the browser
    #[arg(long, value_name = "BOOL", display_order = 0)]
    pub kill_browser_process_on_close: Option<bool>,

    /// Kill all executing browser instances before starting the test(s)
    #[arg(long, value_name = "BOOL", display_order = 0)]
    pub kill_browsers_before_start: Option<bool>,

    /// Launch and reuse a single browser instance for all tests until Manager.Dispose is called
    #[arg(long, value_name = "BOOL", display_order = 0)]
    pub recycle_browser: Option<bool>,

    /// Use the web browser extension to automate the browser (Chrome and Edge only)
    #[arg(long, value_name = "BOOL", display_order = 0)]
    pub use_browser_extension: Option<bool>,

    /// Enable WebComponents support
    #[arg(long, value_name = "BOOL", display_order = 0)]
    pub web_components: Option<bool>,

    //----------------------------
    // Web.Browser-specific
    //----------------------------
    // Note: the following is currently not an official Test Studio setting, it is unique to tsrun.
    /// Use a custom Chrome profile
    ///
    /// Note: tsrun needs to modify the Windows registry when speciyfing this setting, so you may
    /// need to run as an administrator account.
    #[arg(long, value_name = "NAME", display_order = 0)]
    pub chrome_profile_name: Option<String>,

    //----------------------------
    // Web.HttpProxy
    //----------------------------
    /// Use the built-in HTTP proxy during automation
    ///
    /// Enabling Silverlight will automatically enable the HTTP proxy.
    #[arg(long, value_name = "BOOL", display_order = 0)]
    pub use_http_proxy: Option<bool>,

    //----------------------------
    // Web.Logging
    //----------------------------
    /// Enable web script logging
    #[arg(long, value_name = "BOOL", display_order = 0)]
    pub enable_script_logging: Option<bool>,

    /// Use a high level of tracing output for the HTTP proxy
    #[arg(long, value_name = "BOOL", display_order = 0)]
    pub verbose_http_proxy: Option<bool>,

    //----------------------------
    // Web.MultiBrowser Execution
    //----------------------------
    /// A comma separated list of browsers to execute the test(s) against
    ///
    /// When this setting is specified, use-multi-browser-execution is automatically enabled.
    #[arg(
        long,
        value_name = "BROWSERS",
        value_delimiter = ',',
        display_order = 0
    )]
    pub executing_browsers: Option<Vec<ExecutingBrowsers>>,

    /// Use multi-browser execution
    ///
    /// This setting is automatically enabled whenever executing_browsers is specified.
    #[arg(long, value_name = "BOOL", display_order = 0)]
    pub use_multi_browser_execution: Option<bool>,

    //----------------------------
    // Web.Navigation
    //----------------------------
    /// The BaseURL to use for all NavigateTo commands
    ///
    /// When set, NavigateTo steps should use a relative URL, i.e. "~/default.aspx".
    #[arg(long, value_name = "URL", display_order = 0)]
    pub base_url: Option<String>,

    //----------------------------
    // Web.Silverlight
    //----------------------------
    /// Enable Silverlight automation
    ///
    /// Enabling Silverlight will automatically enable the HTTP proxy
    #[arg(long, value_name = "BOOL", display_order = 0)]
    pub enable_silverlight: Option<bool>,

    /// The web address or local directory from which to load a Silverlight application
    #[arg(long, value_name = "PATH", display_order = 0)]
    pub silverlight_application_path: Option<String>,

    /// The amount of time to wait for a Silverlight application to load before timing out
    #[arg(long, value_name = "MSEC", display_order = 0)]
    pub silverlight_connect_timeout: Option<u32>,

    //----------------------------
    // WPF
    //----------------------------
    /// The default application arguments for the WPF tests
    ///
    /// This setting requires wpf-default-application-path to be set also.
    #[arg(
        long,
        value_name = "ARGS",
        requires = "wpf_default_application_path",
        display_order = 0
    )]
    pub wpf_default_application_args: Option<String>,

    /// The default application path for WPF tests
    #[arg(long, value_name = "PATH", display_order = 0)]
    pub wpf_default_application_path: Option<String>,

    //----------------------------
    // Responsive Web
    //----------------------------
    /// The type of device to simulate
    #[arg(long, value_name = "DEVICE", value_enum, display_order = 0)]
    pub responsive_web_device: Option<ResponsiveWebDevice>,

    /// The height of the display device to simulate
    #[arg(long, value_name = "HEIGHT", display_order = 0)]
    pub responsive_web_height: Option<u32>,

    /// The width of the display device to simulate
    #[arg(long, value_name = "WIDTH", display_order = 0)]
    pub responsive_web_width: Option<u32>,

    /// The browser user agent string
    #[arg(long, value_name = "USERAGENT", display_order = 0)]
    pub responsive_user_agent: Option<String>,

    //----------------------------
    // Desktop
    //----------------------------
    /// The default application arguments for desktop tests
    ///
    /// This setting requires desktop-default-application-path to be set also.
    #[arg(
        long,
        value_name = "ARGS",
        requires = "desktop_default_application_path",
        display_order = 0
    )]
    pub desktop_default_application_args: Option<String>,

    /// The default application path for desktop tests
    #[arg(long, value_name = "PATH", display_order = 0)]
    pub desktop_default_application_path: Option<String>,
}

//======================================================================
// Value Enums
//======================================================================
#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ValueEnum)]
pub enum AnnotationMode {
    All,
    NativeOnly,
    CustomOnly,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ValueEnum)]
pub enum Browser {
    Edge = 10,
    Chrome = 7,
    Firefox = 2,
    EdgeHeadless = 12,
    ChromeHeadless = 11,
    IE = 1,
    SilverlightOutOfBrowser = 6,
    /* AspNetHost = 3,
    Designer = 4,
    Safari = 5,
    NativeApp = 8,
    MicrosoftEdge = 9, */
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ValueEnum)]
pub enum ExecutingBrowsers {
    Edge = 8,
    Chrome = 6,
    Firefox = 2,
    EdgeHeadless = 10,
    ChromeHeadless = 9,
    IE = 1,
    Safari = 3,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ValueEnum)]
pub enum LocalWebServer {
    None,
    AspNetDevelopmentServer,
    AspNetDevelopmentServer40,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ValueEnum)]
pub enum RecordingCodec {
    Mjpeg,
    X264,
    Xvid,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ValueEnum)]
pub enum RecordingMode {
    Off,
    OnFail,
    Always,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ValueEnum)]
#[allow(non_camel_case_types)]
pub enum ResponsiveWebDevice {
    Iphone_5_SE,
    Iphone_6_7_8,
    Iphone_6_7_8_Plus,
    IphoneX,
    IphoneXS,
    Iphone_11,
    Ipad,
    IpadPro,
    Pixel_2,
    Pixel_2_XL,
    Pixel_4,
    GalaxyS5,
    GalaxyNote_3,
    GalaxyS20,
    GalaxyNote_10Plus,
    GalaxyFold,
    GalaxyA50,
    GalaxyTabS6,
    MotoG4,
    HuaweiP30Pro,
    HuaweiMate_20_Pro,
    XiaomiMi_3,
    XiaomiRedmiNote_5,
    XiaomiRedmiNote_8,
    Oneplus_7_Pro,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ValueEnum)]
pub enum TelerikComponentsVersion {
    Latest,
    R22023 = 9989,
    R12023SP1 = 9991,
    R12023 = 9992,
    R32022 = 9994,
    R22022SP1 = 9996,
    RR22022 = 9997,
    R12022SP1 = 9998,
    R12022 = 9999,
    R32021 = 10000,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ValueEnum)]
pub enum UnexpectedDailogAction {
    HandleAndFailTest,
    HandleAndContinue,
    DoNotHandle,
}

//======================================================================
// Other types
//======================================================================
pub struct DeviceSettings {
    pub height: u32,
    pub width: u32,
    pub user_agent: &'static str,
}

//======================================================================
// Styles
//======================================================================
pub fn get_styles() -> clap::builder::Styles {
    clap::builder::Styles::styled()
        .usage(
            anstyle::Style::new()
                .bold()
                .underline()
                .fg_color(Some(anstyle::Color::Ansi(anstyle::AnsiColor::Yellow))),
        )
        .header(
            anstyle::Style::new()
                .bold()
                .underline()
                .fg_color(Some(anstyle::Color::Ansi(anstyle::AnsiColor::Yellow))),
        )
        .literal(
            anstyle::Style::new().fg_color(Some(anstyle::Color::Ansi(anstyle::AnsiColor::Green))),
        )
        .invalid(
            anstyle::Style::new()
                .bold()
                .fg_color(Some(anstyle::Color::Ansi(anstyle::AnsiColor::Red))),
        )
        .error(
            anstyle::Style::new()
                .bold()
                .fg_color(Some(anstyle::Color::Ansi(anstyle::AnsiColor::Red))),
        )
        .valid(
            anstyle::Style::new()
                .bold()
                .underline()
                .fg_color(Some(anstyle::Color::Ansi(anstyle::AnsiColor::Green))),
        )
        .placeholder(
            anstyle::Style::new().fg_color(Some(anstyle::Color::Ansi(anstyle::AnsiColor::White))),
        )
}
