mod cli;
mod constants;

use clap::Parser;
use cli::*;
use constants::*;
use std::io::ErrorKind;
use std::path::{Component, PathBuf, Prefix};
use std::process::{Command, ExitStatus};

pub type MyResult<T> = Result<T, Box<dyn std::error::Error + Send + Sync + 'static>>;

pub struct LaunchContext {
    current_dir: PathBuf,
    target_launch_file: PathBuf,
    temp_settings_file: Option<PathBuf>,
    custom_chrome_profile: bool,
    previous_chrome_profile_name: String,
}

//======================================================================
// Main functions
//======================================================================
pub fn run() -> MyResult<i32> {
    let config = Cli::parse();
    if config.debug {
        eprintln!("{:#?}", config);
    }

    let mut context = LaunchContext {
        current_dir: std::env::current_dir()?,
        target_launch_file: PathBuf::new(),
        temp_settings_file: None,
        custom_chrome_profile: false,
        previous_chrome_profile_name: String::new(),
    };
    let mut runner_args = get_runner_args(&mut context, &config)?;

    // Process any settings specified with the Set command
    match &config.command {
        Some(subcommand) => match subcommand {
            Commands::Set(settings) => {
                if let Some(profile_name) = &settings.chrome_profile_name {
                    context.custom_chrome_profile = true;
                    context.previous_chrome_profile_name = set_chrome_profile(profile_name)?;
                }
                context.temp_settings_file = Some(process_settings(settings, &context, &config)?);
            }
        },
        None => {}
    };

    // Add the settings option to the launcher args if necessary
    if let Some(temp_settings_file) = &context.temp_settings_file {
        runner_args.push(format!("settings={}", temp_settings_file.display()));
    } else if let Some(settings) = &config.settings {
        let settings_path = resolve_path(settings, SETTINGS_EXT, &context, &config);
        if config.debug {
            eprintln!("Using settings file {}", settings_path.display());
        }
        runner_args.push(format!("settings={}", settings_path.display()));
    } else {
        // No settings file was specified, check to see if a settings file with the same name
        // as the target launch file exists in the same directory, but only when settings
        // auto-load is enabled.
        if !config.disable_settings_auto_load {
            let settings_path = context.target_launch_file.with_extension(SETTINGS_EXT);
            if settings_path.exists() {
                if config.debug {
                    eprintln!("Using settings file {}", settings_path.display());
                }
                runner_args.push(format!("settings={}", settings_path.display()));
            }
        }
    }

    // Launch the runner
    let result: MyResult<i32> = match launch_runner(&runner_args, &config) {
        Ok(status) => {
            if config.debug {
                eprintln!("{AOTR_FILE_NAME} exited with {status}")
            }
            Ok(status.code().unwrap_or(-1))
        }
        Err(e) => {
            if let Some(io_err) = e.downcast_ref::<std::io::Error>() {
                Err(Box::new(std::io::Error::new(
                    io_err.kind(),
                    format!("Failed to launch {AOTR_FILE_NAME}: {}", e),
                )))
            } else {
                Err(e)
            }
        }
    };

    // Clean up
    if context.custom_chrome_profile {
        restore_chrome_profile(&context)?;
    }
    if let Some(temp_settings_file) = &context.temp_settings_file {
        if let Err(e) =
            std::fs::remove_dir_all(std::path::Path::new(temp_settings_file).parent().unwrap())
        {
            eprintln!("{}", e);
        }
    }

    result
}

pub fn launch_runner(runner_args: &Vec<String>, config: &Cli) -> MyResult<ExitStatus> {
    let create_command = |cmd_path: &PathBuf| {
        let mut cmd = Command::new(cmd_path);
        cmd.args(runner_args);
        if config.debug {
            eprintln!("{:#?}", cmd);
        }
        cmd
    };

    // Use runner_path if it is specified
    if let Some(runner_path) = &config.runner_path {
        Ok(create_command(runner_path).status()?)
    // Otherwise, try launching AOTR relying on default OS path search
    } else {
        let result = create_command(&PathBuf::from(AOTR_FILE_NAME)).status();
        match result {
            Ok(_) => Ok(result?),
            Err(ref e) => {
                // If AOTR is still not found, fallback to BinPath from registry if present, or
                // use hard-coded default location as a last resort.
                if e.kind() == ErrorKind::NotFound {
                    if config.debug {
                        eprintln!("Failed to find {AOTR_FILE_NAME} on OS path");
                    }
                    let hklm = winreg::RegKey::predef(winreg::enums::HKEY_LOCAL_MACHINE);

                    if let Ok(ts_key) = hklm.open_subkey(TS_REG_KEY) {
                        if let Ok(bin_path) = ts_key.get_value::<std::ffi::OsString, _>("BinPath") {
                            let mut runner_path = PathBuf::from(bin_path);
                            runner_path.push(AOTR_FILE_NAME);
                            if config.debug {
                                eprintln!("Using path from registry: {}", runner_path.display());
                            }
                            return Ok(create_command(&runner_path).status()?);
                        }
                    }
                    if config.debug {
                        eprintln!("Failed to find Test Studio BinPath in registry");
                        eprintln!("Falling back to {AOTR_FALLBACK_PATH}");
                    }
                    Ok(create_command(&PathBuf::from(AOTR_FALLBACK_PATH)).status()?)
                } else {
                    Ok(result?)
                }
            }
        }
    }
}

pub fn get_runner_args(context: &mut LaunchContext, config: &Cli) -> MyResult<Vec<String>> {
    let mut args = Vec::new();

    // Canonicalize the root path if a relative path was specified
    let root = match &config.root {
        Some(root) => {
            if root.is_absolute() {
                Some(std::borrow::Cow::from(root))
            } else {
                let absolute_root = root.canonicalize()?;
                // Since fs::canonicalize returns a Rust "verbatim" path (a.k.a. a Windows
                // extended-length path) having a prefix of "\\?\D:\", we need to convert this to a
                // normal fully qualified path beginning with the drive letter, e.g. "D:\...",
                // because the ArtOfTest.Runner cannot handle extended-length paths.
                let mut full_path = PathBuf::new();
                for c in absolute_root.components() {
                    if let Component::Prefix(prefix_component) = c {
                        if let Prefix::VerbatimDisk(disk) = prefix_component.kind() {
                            full_path.push(format!("{}:", disk as char));
                        }
                    } else {
                        full_path.push(c);
                    }
                }
                Some(std::borrow::Cow::from(full_path))
            }
        }
        None => None,
    };

    // Resolve path to test file if specified
    if let Some(test) = &config.test_type.test {
        context.target_launch_file = resolve_path(test, TEST_EXT, context, config);
        args.push(format!("test={}", context.target_launch_file.display()));
    }

    // Resolve path to test list if specified
    if let Some(list) = &config.test_type.list {
        let mut test_list_path = PathBuf::from(list);

        // Append the file extension if necessary
        if test_list_path.extension().is_none() {
            test_list_path.set_extension(TESTLIST_EXT);
        }

        if test_list_path.components().count() == 1 {
            // list potentially contains just a file name
            if let Some(test_list_file_name) = test_list_path.file_name() {
                // Check for the file in the current directory
                let check_path = context.current_dir.join(test_list_file_name);
                if check_path.exists() {
                    test_list_path = check_path;
                } else {
                    // Check for the file in a designated subdirectory of the current dir
                    let check_path = context
                        .current_dir
                        .join(TESTLIST_DIR)
                        .join(test_list_file_name);
                    if check_path.exists() {
                        test_list_path = check_path;
                    } else if let Some(root_path) = &root {
                        // Check for the file in the project root directory
                        let check_path = root_path.join(test_list_file_name);
                        if check_path.exists() {
                            test_list_path = check_path;
                        } else {
                            // Check for the file in a designated subdirectory of the root dir
                            let check_path = root_path.join(TESTLIST_DIR).join(test_list_file_name);
                            if check_path.exists() {
                                test_list_path = check_path;
                            }
                        }
                    }
                }
            }
        }
        args.push(format!("list={}", test_list_path.display()));
        context.target_launch_file = test_list_path;
    }

    if config.xml {
        args.push("xml".to_string());
    }

    if config.html {
        args.push("html".to_string());
    }

    if config.junit {
        args.push("junit".to_string());
    }

    if config.junitstep {
        args.push("junitstep".to_string());
    }

    if let Some(out) = &config.out {
        args.push(format!("out={out}"));
    }

    if let Some(result) = &config.result {
        args.push(format!("result={result}"));
    }

    if let Some(root_path) = &root {
        args.push(format!("root={}", root_path.display()));
    }

    if config.persist_on_each_step {
        args.push(format!("persistOnEachStep={}", config.persist_on_each_step));
    }

    Ok(args)
}

pub fn resolve_path(
    target_path: &PathBuf,
    file_ext: &str,
    context: &LaunchContext,
    config: &Cli,
) -> PathBuf {
    let mut full_path = PathBuf::from(target_path);

    // Append the file extension if necessary
    if full_path.extension().is_none() {
        full_path.set_extension(file_ext);
    }

    if full_path.components().count() == 1 {
        // target potentially contains just a file name
        if let Some(target_file_name) = full_path.file_name() {
            // Check for the file in the current directory
            let check_path = context.current_dir.join(target_file_name);
            if check_path.exists() {
                full_path = check_path;
            } else if let Some(root_path) = &config.root {
                // Check for the file in the project root directory
                let check_path = root_path.join(target_file_name);
                if check_path.exists() {
                    full_path = check_path;
                }
            }
        }
    }
    full_path
}

pub fn process_settings(
    settings: &SetArgs,
    context: &LaunchContext,
    config: &Cli,
) -> MyResult<PathBuf> {
    // Read in the settings file
    let mut data = match &config.settings {
        Some(settings) => {
            // Read the settings file specified on the command line
            let settings_path = resolve_path(settings, SETTINGS_EXT, context, config);
            let content = std::fs::read_to_string(&settings_path)?;
            if config.debug {
                eprintln!("Loading settings from {}", settings_path.display());
            }
            json::parse(&content)?
        }
        None => {
            // No settings file was specified, check to see if a settings file with the same name
            // as the target launch file exists in the same directory, but only when settings
            // auto-load is enabled.
            let mut data = json::object! {
                Settings: {
                    __type: "ArtOfTest.WebAii.Core.Settings",
                }
            };
            if !config.disable_settings_auto_load {
                let settings_path = context.target_launch_file.with_extension(SETTINGS_EXT);
                if let Ok(settings) = std::fs::read_to_string(&settings_path) {
                    if config.debug {
                        eprintln!("Loading settings from {}", settings_path.display());
                    }
                    data = json::parse(&settings)?
                }
            }
            data
        }
    };

    //----------------------------
    // General settings
    //----------------------------
    if let Some(value) = settings.annotate_execution {
        data["Settings"]["__value"]["AnnotateExecution"] = value.into();
    }
    if let Some(value) = settings.annotation_mode {
        data["Settings"]["__value"]["AnnotationMode"] = (value as u16).into();
    }
    if let Some(value) = settings.execute_in_development_tests {
        data["Settings"]["__value"]["ExecuteInDevelopmentTests"] = value.into();
    }
    if let Some(value) = settings.simulated_mouse_move_speed {
        data["Settings"]["__value"]["SimulatedMouseMoveSpeed"] = value.into();
    }
    if let Some(value) = settings.client_ready_timeout {
        data["Settings"]["__value"]["ClientReadyTimeout"] = value.into();
    }
    if let Some(value) = settings.disable_dialog_monitoring {
        data["Settings"]["__value"]["DisableDialogMonitoring"] = value.into();
    }
    if let Some(value) = settings.element_image_search_delay {
        data["Settings"]["__value"]["ElementImageSearchDelay"] = value.into();
    }
    if let Some(value) = settings.element_image_search_timeout {
        data["Settings"]["__value"]["ElementImageSearchTimeout"] = value.into();
    }
    if let Some(value) = settings.element_wait_timeout {
        data["Settings"]["__value"]["ElementWaitTimeout"] = value.into();
    }
    if let Some(value) = settings.enable_image_search {
        data["Settings"]["__value"]["EnableImageSearch"] = value.into();
    }
    if let Some(value) = settings.execute_command_timeout {
        data["Settings"]["__value"]["ExecuteCommandTimeout"] = value.into();
    }
    if let Some(value) = settings.execution_delay {
        data["Settings"]["__value"]["ExecutionDelay"] = value.into();
    }
    if let Some(value) = settings.rerun_failed_tests {
        data["Settings"]["__value"]["RerunFailedTests"] = value.into();
    }
    if let Some(value) = settings.runner_response_timeout {
        data["Settings"]["__value"]["RunnerResponseTimeout"] = value.into();
    }
    if let Some(value) = settings.scroll_on_image_search {
        data["Settings"]["__value"]["ScrollOnImageSearch"] = value.into();
    }
    if let Some(value) = settings.search_by_image_first {
        data["Settings"]["__value"]["SearchByImageFirst"] = value.into();
    }
    if let Some(value) = settings.unexpected_dialog_action {
        data["Settings"]["__value"]["UnexpectedDialogAction"] = (value as u16).into();
    }
    if let Some(value) = settings.wait_check_interval {
        data["Settings"]["__value"]["WaitCheckInterval"] = value.into();
    }
    if let Some(value) = settings.x_multi_mgr {
        data["Settings"]["__value"]["XMultiMgr"] = value.into();
    }
    if let Some(value) = settings.create_log_file {
        data["Settings"]["__value"]["CreateLogFile"] = value.into();
    }
    if let Some(value) = settings.log_annotations {
        data["Settings"]["__value"]["LogAnnotations"] = value.into();
    }
    if let Some(value) = &settings.log_location {
        data["Settings"]["__value"]["LogLocation"] = value.clone().into();
    }
    if let Some(value) = settings.recording_codec {
        data["Settings"]["__value"]["RecordingCodec"] = (value as u16).into();
    }
    if let Some(value) = settings.recording_fps {
        data["Settings"]["__value"]["RecordingFPS"] = value.into();
    }
    if let Some(value) = settings.recording_mode {
        data["Settings"]["__value"]["RecordingMode"] = (value as u16).into();
    }
    if let Some(value) = &settings.recording_output_location {
        data["Settings"]["__value"]["RecordingOutputLocation"] = value.clone().into();
    }
    if let Some(value) = settings.recording_scale {
        data["Settings"]["__value"]["RecordingScale"] = value.into();
    }
    if let Some(value) = settings.recording_size_limit {
        data["Settings"]["__value"]["RecordingSizeLimit"] = value.into();
    }
    if let Some(value) = settings.telerik_components_version {
        data["Settings"]["__value"]["TelerikComponentsVersion"] = (value as u16).into();
    }

    //----------------------------
    // Web settings
    //----------------------------
    if data["Settings"]["__value"]["Web"].is_null() {
        data["Settings"]["__value"]["Web"]["__type"] =
            "ArtOfTest.WebAii.Core.Settings+WebSettings".into();
        data["Settings"]["__value"]["Web"]["__value"] = json::object! {};
    }
    if let Some(value) = settings.asp_net_dev_server_port {
        data["Settings"]["__value"]["Web"]["__value"]["AspNetDevServerPort"] = value.into();
    }
    if let Some(value) = settings.local_web_server {
        data["Settings"]["__value"]["Web"]["__value"]["LocalWebServer"] = (value as u16).into();
    }
    if let Some(value) = &settings.web_app_physical_path {
        data["Settings"]["__value"]["Web"]["__value"]["WebAppPhysicalPath"] = value.clone().into();
    }
    if let Some(value) = settings.auto_calibrate_browsers {
        data["Settings"]["__value"]["Web"]["__value"]["AutoCalibrateBrowsers"] = value.into();
    }
    if let Some(value) = settings.browser {
        data["Settings"]["__value"]["Web"]["__value"]["DefaultBrowser"] = (value as u16).into();
    }
    if let Some(value) = settings.enable_ui_less_request_viewing {
        data["Settings"]["__value"]["Web"]["__value"]["EnableUILessRequestViewing"] = value.into();
    }
    if let Some(value) = settings.kill_browser_process_on_close {
        data["Settings"]["__value"]["Web"]["__value"]["KillBrowserProcessOnClose"] = value.into();
    }
    if let Some(value) = settings.kill_browsers_before_start {
        data["Settings"]["__value"]["Web"]["__value"]["KillBrowsersBeforeStart"] = value.into();
    }
    if let Some(value) = settings.recycle_browser {
        data["Settings"]["__value"]["Web"]["__value"]["RecycleBrowser"] = value.into();
    }
    if let Some(value) = settings.use_browser_extension {
        data["Settings"]["__value"]["Web"]["__value"]["UseBrowserExtension"] = value.into();
    } else {
        // Always enable this setting if a custom Chrome profile was specified since the custom
        // profile is only applicable to tests using the browser extension.
        if context.custom_chrome_profile {
            data["Settings"]["__value"]["Web"]["__value"]["UseBrowserExtension"] = true.into();
        }
    }
    if let Some(value) = settings.web_components {
        data["Settings"]["__value"]["Web"]["__value"]["WebComponents"] = value.into();
    }
    if let Some(value) = settings.use_http_proxy {
        data["Settings"]["__value"]["Web"]["__value"]["UseHttpProxy"] = value.into();
    }
    if let Some(value) = settings.enable_script_logging {
        data["Settings"]["__value"]["Web"]["__value"]["EnableScriptLogging"] = value.into();
    }
    if let Some(value) = settings.verbose_http_proxy {
        data["Settings"]["__value"]["Web"]["__value"]["VerboseHttpProxy"] = value.into();
    }
    if let Some(value) = &settings.executing_browsers {
        let values: Vec<u16> = value.iter().map(|v| *v as u16).collect();
        data["Settings"]["__value"]["Web"]["__value"]["ExecutingBrowsers"] = values.into();
        // Enable multi-browser execution whenever ExecutingBrowsers is specified, but this may
        // still be overriden by setting UseMultiBrowserExecution explicitly.
        data["Settings"]["__value"]["Web"]["__value"]["UseMultiBrowserExecution"] = true.into();
    }
    if let Some(value) = settings.use_multi_browser_execution {
        data["Settings"]["__value"]["Web"]["__value"]["UseMultiBrowserExecution"] = value.into();
    }
    if let Some(value) = &settings.base_url {
        data["Settings"]["__value"]["Web"]["__value"]["BaseUrl"] = value.clone().into();
    }
    if let Some(value) = settings.enable_silverlight {
        data["Settings"]["__value"]["Web"]["__value"]["EnableSilverlight"] = value.into();
    }
    if let Some(value) = &settings.silverlight_application_path {
        data["Settings"]["__value"]["Web"]["__value"]["SilverlightApplicationPath"] =
            value.clone().into();
    }
    if let Some(value) = settings.silverlight_connect_timeout {
        data["Settings"]["__value"]["Web"]["__value"]["SilverlightConnectTimeout"] = value.into();
    }

    //----------------------------
    // WPF settings
    //----------------------------
    if data["Settings"]["__value"]["Wpf"].is_null() {
        data["Settings"]["__value"]["Wpf"]["__type"] =
            "ArtOfTest.WebAii.Core.Settings+WpfSettings".into();
        data["Settings"]["__value"]["Wpf"]["__value"] = json::object! {};
    }
    if let Some(value) = &settings.wpf_default_application_args {
        data["Settings"]["__value"]["Wpf"]["__value"]["DefaultApplicationArgs"] =
            value.clone().into();
    }
    if let Some(value) = &settings.wpf_default_application_path {
        data["Settings"]["__value"]["Wpf"]["__value"]["DefaultApplicationPath"] =
            value.clone().into();
    }

    //----------------------------
    // ResponsiveWeb settings
    //----------------------------
    if data["Settings"]["__value"]["ResponsiveWeb"].is_null() {
        data["Settings"]["__value"]["ResponsiveWeb"]["__type"] =
            "ArtOfTest.WebAii.Core.Settings+ResponsiveWebSettings".into();
        data["Settings"]["__value"]["ResponsiveWeb"]["__value"] = json::object! {};
    }
    if let Some(value) = settings.responsive_web_device {
        let device = get_device_settings(value);
        data["Settings"]["__value"]["ResponsiveWeb"]["__value"]["Height"] = device.height.into();
        data["Settings"]["__value"]["ResponsiveWeb"]["__value"]["Width"] = device.width.into();
        data["Settings"]["__value"]["ResponsiveWeb"]["__value"]["UserAgent"] =
            device.user_agent.into();
    }
    if let Some(value) = settings.responsive_web_height {
        data["Settings"]["__value"]["ResponsiveWeb"]["__value"]["Height"] = value.into();
    }
    if let Some(value) = settings.responsive_web_width {
        data["Settings"]["__value"]["ResponsiveWeb"]["__value"]["Width"] = value.into();
    }
    if let Some(value) = &settings.responsive_user_agent {
        data["Settings"]["__value"]["ResponsiveWeb"]["__value"]["UserAgent"] = value.clone().into();
    }

    //----------------------------
    // Desktop settings
    //----------------------------
    if data["Settings"]["__value"]["Desktop"].is_null() {
        data["Settings"]["__value"]["Desktop"]["__type"] =
            "ArtOfTest.WebAii.Core.Settings+DesktopSettings".into();
        data["Settings"]["__value"]["Desktop"]["__value"] = json::object! {};
    }
    if let Some(value) = &settings.desktop_default_application_args {
        data["Settings"]["__value"]["Desktop"]["__value"]["DefaultApplicationArgs"] =
            value.clone().into();
    }
    if let Some(value) = &settings.desktop_default_application_path {
        data["Settings"]["__value"]["Desktop"]["__value"]["DefaultApplicationPath"] =
            value.clone().into();
    }

    if config.debug {
        eprintln!("{:#}", data);
    }

    let temp_dir = tempfile::TempDir::with_prefix("tsrun_")?;
    let temp_path = temp_dir.into_path().join(TMP_SETTINGS_FILE);
    std::fs::write(&temp_path, data.dump())?;

    Ok(temp_path)
}

pub fn get_device_settings(device: ResponsiveWebDevice) -> DeviceSettings {
    use ResponsiveWebDevice::*;

    match device {
        Iphone_5_SE => DeviceSettings { height: 568, width: 320, user_agent: "Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_1 like Mac OS X) AppleWebKit/603.1.30 (KHTML, like Gecko) Version/10.0 Mobile/14E304 Safari/602.1"},
        Iphone_6_7_8 => DeviceSettings { height: 667, width: 375, user_agent: "Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1"},
        Iphone_6_7_8_Plus => DeviceSettings { height: 568, width: 414, user_agent: "Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1"},
        IphoneX => DeviceSettings { height: 812, width: 375, user_agent: "Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1"},
        IphoneXS => DeviceSettings { height: 812, width: 375, user_agent: "Mozilla/5.0 (iPhone; CPU iPhone OS 12_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0 Mobile/15E148 Safari/604.1"},
        Iphone_11 => DeviceSettings { height: 896, width: 414, user_agent: "Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1. 38 (KHTML, like Gecko) Version/11.0 Mobile/15A356 Safari/604.1"},
        Ipad => DeviceSettings { height: 1024, width: 768, user_agent: "Mozilla/5.0 (iPad; CPU OS 11_0 like Mac OS X) AppleWebKit/604.1.34 (KHTML, like Gecko) Version/11.0 Mobile/15A5341f Safari/604.1"},
        IpadPro => DeviceSettings { height: 1366, width: 1024, user_agent: "Mozilla/5.0 (iPad; CPU OS 11_0 like Mac OS X) AppleWebKit/604.1.34 (KHTML, like Gecko) Version/11.0 Mobile/15A5341f Safari/604.1"},
        Pixel_2 => DeviceSettings { height: 731, width: 411, user_agent: "Mozilla/5.0 (Linux; Android 8.0; Pixel 2 Build/OPD3.170816.012) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Mobile Safari/537.36"},
        Pixel_2_XL => DeviceSettings { height: 823, width: 411, user_agent: "Mozilla/5.0 (Linux; Android 8.0.0; Pixel 2 XL Build/OPD1.170816.004) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Mobile Safari/537.36"},
        Pixel_4 => DeviceSettings { height: 869, width: 411, user_agent: "Mozilla/5.0 (Linux; Android 10; Pixel 4 XL) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.62 Mobile Safari/537.36"},
        GalaxyS5 => DeviceSettings { height: 640, width: 360, user_agent: "Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Mobile Safari/537.36"},
        GalaxyNote_3 => DeviceSettings { height: 640, width: 360, user_agent: "Mozilla/5.0 (Linux; U; Android 4.3; en-us; SM-N900T Build/JSS15J) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30"},
        GalaxyS20 => DeviceSettings { height: 800, width: 360, user_agent: "Mozilla/5.0 (Linux; Android 10; SM-G981B) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.162 Mobile Safari/537.36"},
        GalaxyNote_10Plus => DeviceSettings { height: 869, width: 412, user_agent: "Mozilla/5.0 (Linux; Android 9; SM-N976V) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.89 Mobile Safari/537.36"},
        GalaxyFold => DeviceSettings { height: 710, width: 586, user_agent: "Mozilla/5.0 (Linux; Android 9; SAMSUNG SM-F900U Build/PPR1.180610.011) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/9.2 Chrome/67.0.3396.87 Mobile Safari/537.36"},
        GalaxyA50 => DeviceSettings { height: 892, width: 412, user_agent: "Mozilla/5.0 (Linux; Android 9; SM-A505F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.105 Mobile Safari/537.36"},
        GalaxyTabS6 => DeviceSettings { height: 1280, width: 800, user_agent: "Mozilla/5.0 (Linux; Android 9; SAMSUNG SM-T865) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/10.1 Chrome/71.0.3578.99 Safari/537.36"},
        MotoG4 => DeviceSettings { height: 640, width: 360, user_agent: "Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4)) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Mobile Safari/537.36"},
        HuaweiP30Pro => DeviceSettings { height: 780, width: 360, user_agent: "Mozilla/5.0 (Linux; Android 9; VOG-L29) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.90 Mobile Safari/537.36"},
        HuaweiMate_20_Pro => DeviceSettings { height: 780, width: 360, user_agent: "Mozilla/5.0 (Linux; Android 9; LYA-AL00 Build/HUAWEILYA-AL00) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Mobile Safari/537.36"},
        XiaomiMi_3 => DeviceSettings { height: 640, width: 360, user_agent: "Mozilla/5.0 (Linux; U; Android 4.4.4; en-gb; MI 3W Build/KTU84P) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/39.0.0.0 Mobile Safari/537.36 XiaoMi/MiuiBrowser/2.1.1"},
        XiaomiRedmiNote_5 => DeviceSettings { height: 851, width: 393, user_agent: "Mozilla/5.0 (Linux; Android 8.1.0; Redmi Note 5 Build/OPM1.171019.019; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/69.0.3497.100 Mobile Safari/537.36"},
        XiaomiRedmiNote_8 => DeviceSettings { height: 851, width: 393, user_agent: "Mozilla/5.0 (Linux; U; Android 9; es-es; Redmi Note 8T Build/PKQ1.190616.001) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/71.0.3578.141 Mobile Safari/537.36 XiaoMi/MiuiBrowser/12.1.5-g"},
        Oneplus_7_Pro => DeviceSettings { height: 892, width: 412, user_agent: "Mozilla/5.0 (Linux; Android 10; GM1913) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Mobile Safari/537.36"},
    }
}

pub fn set_chrome_profile(profile_name: &str) -> MyResult<String> {
    use winreg::enums::*;

    let hklm = winreg::RegKey::predef(HKEY_LOCAL_MACHINE);
    let ts_key = hklm.open_subkey_with_flags(TS_REG_KEY, KEY_READ | KEY_WRITE)?;

    let previous_profile_name = match ts_key.get_value::<String, _>(CHROME_PROFILE_SETTING) {
        Ok(current_profile_name) => current_profile_name,
        Err(_) => String::new(),
    };

    ts_key.set_value(CHROME_PROFILE_SETTING, &profile_name)?;

    Ok(previous_profile_name)
}

pub fn restore_chrome_profile(context: &LaunchContext) -> MyResult<()> {
    use winreg::enums::*;

    let hklm = winreg::RegKey::predef(HKEY_LOCAL_MACHINE);
    let ts_key = hklm.open_subkey_with_flags(TS_REG_KEY, KEY_READ | KEY_WRITE)?;

    if context.previous_chrome_profile_name.is_empty() {
        ts_key.delete_value(CHROME_PROFILE_SETTING)?;
    } else {
        ts_key.set_value(
            CHROME_PROFILE_SETTING,
            &context.previous_chrome_profile_name,
        )?;
    }

    Ok(())
}
