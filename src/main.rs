fn main() {
    match tsrun::run() {
        Ok(exit_code) => std::process::exit(exit_code),
        Err(e) => {
            eprintln!("{}", e);
            std::process::exit(-1);
        }
    }
}
