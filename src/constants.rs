pub const AOTR_FALLBACK_PATH: &str =
    r"C:\Program Files (x86)\Progress\Test Studio\Bin\ArtOfTest.Runner.exe";
pub const AOTR_FILE_NAME: &str = "ArtOfTest.Runner.exe";
pub const CHROME_PROFILE_SETTING: &str = "ChromeProfileName";
pub const SETTINGS_EXT: &str = "json";
pub const TEST_EXT: &str = "tstest";
pub const TESTLIST_EXT: &str = "aiilist";
pub const TESTLIST_DIR: &str = "TestLists";
pub const TMP_SETTINGS_FILE: &str = "tsrun_settings.json";
pub const TS_REG_KEY: &str = r"SOFTWARE\WOW6432Node\Telerik\TestStudio";
